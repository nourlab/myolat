const gulp = require('gulp');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');

function css(cb) {
    // place code for your default task here
    cb();
    return gulp.src('myolat/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('../../../target/classes/static/themes/myolat'));
}

exports.default = css;
