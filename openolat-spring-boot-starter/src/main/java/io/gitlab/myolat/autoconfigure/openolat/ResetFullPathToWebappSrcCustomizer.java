package io.gitlab.myolat.autoconfigure.openolat;

import org.olat.core.util.WebappHelper;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

public class ResetFullPathToWebappSrcCustomizer implements BeanPostProcessor {

  @Override
  public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
    if (bean instanceof WebappHelper) {
      resetFullPathToWebappSrc((WebappHelper) bean);
    }
    return null;
  }

  /**
   * This method sets fullPathToWebappSrc to an empty string to fix warning 'File exists but not
   * mapped using version' when using custom themes.
   *
   * @param webappHelper The WebappHelper bean.
   */
  private void resetFullPathToWebappSrc(WebappHelper webappHelper) {
    webappHelper.setFullPathToWebappSrc("");
  }
}
