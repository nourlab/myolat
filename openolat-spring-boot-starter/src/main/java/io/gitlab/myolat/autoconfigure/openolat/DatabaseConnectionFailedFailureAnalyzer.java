package io.gitlab.myolat.autoconfigure.openolat;

import lombok.extern.slf4j.Slf4j;
import org.postgresql.util.PSQLException;
import org.springframework.boot.diagnostics.AbstractFailureAnalyzer;
import org.springframework.boot.diagnostics.FailureAnalysis;

/**
 * This {@code FailureAnalyzer} prints a human-friendly error message when no database is available.
 *
 * @author Christian Schweizer
 * @see org.springframework.boot.diagnostics.FailureAnalyzer
 * @since 0.0.1
 */
@Slf4j
public class DatabaseConnectionFailedFailureAnalyzer
    extends AbstractFailureAnalyzer<PSQLException> {

  @Override
  protected FailureAnalysis analyze(Throwable rootFailure, PSQLException cause) {
    // https://www.postgresql.org/docs/11/errcodes-appendix.html#ERRCODES-TABLE
    if (cause.getSQLState().equals("08001")) {
      return new FailureAnalysis(
          "Unable to connect to database.", "Make sure a database is up and running!", cause);
    }
    return null;
  }
}
