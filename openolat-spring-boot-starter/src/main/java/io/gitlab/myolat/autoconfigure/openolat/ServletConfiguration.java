package io.gitlab.myolat.autoconfigure.openolat;

import org.apache.cxf.transport.servlet.CXFServlet;
import org.olat.commons.calendar.ICalServlet;
import org.olat.core.commons.services.notifications.PersonalRSSServlet;
import org.olat.core.servlets.OpenOLATServlet;
import org.olat.core.servlets.StaticServlet;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.unit.DataSize;

import javax.servlet.MultipartConfigElement;

@Configuration
public class ServletConfiguration {

  @Bean
  public ServletRegistrationBean<OpenOLATServlet> openOlatServlet(
      MultipartConfigElement multipartConfigElement) {
    ServletRegistrationBean<OpenOLATServlet> registrationBean = new ServletRegistrationBean<>();
    registrationBean.setServlet(new OpenOLATServlet());
    registrationBean.setName("openolatservlet");
    registrationBean.setLoadOnStartup(1);
    registrationBean.setMultipartConfig(multipartConfigElement);
    registrationBean.addUrlMappings("/", "/*");
    return registrationBean;
  }

  @Bean
  public ServletRegistrationBean<StaticServlet> staticServlet() {
    ServletRegistrationBean<StaticServlet> registrationBean = new ServletRegistrationBean<>();
    registrationBean.setServlet(new StaticServlet());
    registrationBean.setName("rawservlet");
    registrationBean.setLoadOnStartup(1);
    registrationBean.addUrlMappings("/raw/*");
    return registrationBean;
  }

  @Bean
  public ServletRegistrationBean<CXFServlet> cxfServlet() {
    ServletRegistrationBean<CXFServlet> registrationBean = new ServletRegistrationBean<>();
    registrationBean.setServlet(new CXFServlet());
    registrationBean.setName("RESTServletAdaptor");
    registrationBean.setLoadOnStartup(1);
    registrationBean.setMultipartConfig(createMultipartConfigElement(DataSize.ofBytes(10240L)));
    registrationBean.addUrlMappings("/restapi/*");
    return registrationBean;
  }

  @Bean
  public ServletRegistrationBean<PersonalRSSServlet> personalRssServlet() {
    ServletRegistrationBean<PersonalRSSServlet> registrationBean = new ServletRegistrationBean<>();
    registrationBean.setServlet(new PersonalRSSServlet());
    registrationBean.setName("rss");
    registrationBean.setLoadOnStartup(3);
    registrationBean.addUrlMappings("/rss/*");
    return registrationBean;
  }

  @Bean
  public ServletRegistrationBean<ICalServlet> iCalServlet() {
    ServletRegistrationBean<ICalServlet> registrationBean = new ServletRegistrationBean<>();
    registrationBean.setServlet(new ICalServlet());
    registrationBean.setName("ical");
    registrationBean.setLoadOnStartup(3);
    registrationBean.addUrlMappings("/ical/*");
    return registrationBean;
  }

  private MultipartConfigElement createMultipartConfigElement(DataSize fileSizeThreshold) {
    MultipartConfigFactory factory = new MultipartConfigFactory();
    factory.setFileSizeThreshold(fileSizeThreshold);
    return factory.createMultipartConfig();
  }
}
