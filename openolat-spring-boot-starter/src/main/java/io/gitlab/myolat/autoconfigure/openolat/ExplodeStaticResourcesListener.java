package io.gitlab.myolat.autoconfigure.openolat;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.olat.core.util.WebappHelper;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.io.Resource;
import org.springframework.util.StopWatch;
import org.springframework.web.context.support.ServletContextResourcePatternResolver;

/**
 * Event listener that explodes all static resources (classpath entries in a directory named <code>
 * static</code> to the context directory of the embedded Tomcat server. This is usually a temporary
 * directory that gets created when starting the application.
 *
 * @author Christian Schweizer
 * @since 0.0.1
 */
@Slf4j
public class ExplodeStaticResourcesListener {

  @EventListener
  public void explodeStaticResources(ContextRefreshedEvent event) {
    StopWatch watch = new StopWatch();
    watch.start();
    List<Resource> staticResources;
    if (WebappHelper.getContextRoot() != null) {
      staticResources = findResources(event.getApplicationContext());
      log.info("Found {} static resources", staticResources.size());
      staticResources.forEach(
          resource -> {
            try {
              log.debug("Exploding static resource {}", resource);
              int i = resource.getURL().getPath().indexOf("static");
              if (i != -1) {
                String relativePath = resource.getURL().getPath().substring(i);
                Path absolutePath = Paths.get(WebappHelper.getContextRoot(), relativePath);
                Files.createDirectories(absolutePath.getParent());
                Files.copy(resource.getInputStream(), absolutePath);
              }
            } catch (IOException e) {
              log.warn("An error occurred during exploding static resource: " + e.getMessage(), e);
            }
          });
    } else {
      staticResources = Collections.emptyList();
    }
    watch.stop();
    log.info(
        "{} static resources exploded in {} seconds",
        staticResources.size(),
        watch.getTotalTimeSeconds());
  }

  private List<Resource> findResources(ApplicationContext applicationContext) {
    try {
      ServletContextResourcePatternResolver resolver =
          new ServletContextResourcePatternResolver(applicationContext);
      Resource[] resources = resolver.getResources("classpath*:/static/**/*.*");
      return Arrays.stream(resources).collect(Collectors.toList());
    } catch (IOException e) {
      log.warn("An error occurred while resolving resources: " + e.getMessage(), e);
      return Collections.emptyList();
    }
  }
}
