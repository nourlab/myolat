# MyOlat

An extension for [OpenOlat](https://github.com/OpenOLAT/OpenOLAT).

## Usage

### Docker

MyOlat can run as a Docker container. The Docker image is built by the
[Spring Boot Maven Plugin](https://docs.spring.io/spring-boot/docs/current/maven-plugin/reference/htmlsingle/#build-image).

1. Build the Docker image.

       ./mvnw -DskipTests -DskipSeleniumTests --activate-profiles docker clean verify

1. Start Docker container.

       docker-compose up --detach

1. View container logs.

       docker-compose logs --follow app

1. Stop and delete Docker container.

       docker-compose down --remove-orphans

## License

The code is licensed under the terms of the [Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0).
