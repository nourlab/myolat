package io.gitlab.myolat.autoconfigure.openolat;

import org.olat.user.DefaultUser;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

import java.util.ArrayList;
import java.util.List;

public class TestUsersCustomizer implements BeanPostProcessor {

  @Override
  @SuppressWarnings("NullableProblems")
  public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
    if (beanName.equals("testUsers") && bean instanceof List) {
      List<DefaultUser> testUsers = new ArrayList<>();
      for (int i = 1; i <= 4; i++) {
        DefaultUser user = new DefaultUser("admin" + i);
        user.setFirstName("Test");
        user.setLastName("Administrator " + i);
        user.setEmail("admin" + i + "@example.org");
        user.setPassword("admin0" + i);
        user.setLanguage("de");
        user.setAdmin(true);
        user.setSysAdmin(true);
      }
      return testUsers;
    }
    return bean;
  }
}
