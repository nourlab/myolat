package io.gitlab.myolat.autoconfigure.openolat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.olat.core.util.WebappHelper;

import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class ResetFullPathToWebappSrcCustomizerTest {

  private ResetFullPathToWebappSrcCustomizer customizer;

  @BeforeEach
  void setUp() {
    customizer = new ResetFullPathToWebappSrcCustomizer();
  }

  @Test
  void postProcessAfterInitialization(@Mock WebappHelper webappHelper) {
    customizer.postProcessAfterInitialization(webappHelper, "org.olat.core.util.WebappHelper");
    verify(webappHelper).setFullPathToWebappSrc("");
  }
}
