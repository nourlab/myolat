package io.gitlab.myolat.autoconfigure.openolat;

import org.olat.core.dispatcher.mapper.MapperSessionListener;
import org.olat.restapi.security.RestApiSessionListener;
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ServletListenerConfiguration {

  @Bean
  public ServletListenerRegistrationBean<RestApiSessionListener> restApiSessionListener() {
    return new ServletListenerRegistrationBean<>(new RestApiSessionListener());
  }

  @Bean
  public ServletListenerRegistrationBean<MapperSessionListener> mapperSessionListener() {
    return new ServletListenerRegistrationBean<>(new MapperSessionListener());
  }
}
