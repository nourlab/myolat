package io.gitlab.myolat.autoconfigure.openolat;

import org.olat.core.servlets.HeadersFilter;
import org.olat.resource.accesscontrol.provider.paypal.PaypalIPNFilter;
import org.olat.restapi.security.RestApiLoginFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ServletFilterConfiguration {

  @Bean
  public FilterRegistrationBean<PaypalIPNFilter> paypalIPNFilter() {
    FilterRegistrationBean<PaypalIPNFilter> registrationBean = new FilterRegistrationBean<>();
    registrationBean.setFilter(new PaypalIPNFilter());
    registrationBean.setName("PaypalIPNFilter");
    registrationBean.addUrlPatterns("/paypal/*");
    return registrationBean;
  }

  @Bean
  public FilterRegistrationBean<RestApiLoginFilter> restApiLoginFilter() {
    FilterRegistrationBean<RestApiLoginFilter> registrationBean = new FilterRegistrationBean<>();
    registrationBean.setFilter(new RestApiLoginFilter());
    registrationBean.setName("RESTApiLoginFilter");
    registrationBean.addUrlPatterns("/restapi/*");
    return registrationBean;
  }

  @Bean
  public FilterRegistrationBean<HeadersFilter> headersFilter() {
    FilterRegistrationBean<HeadersFilter> registrationBean = new FilterRegistrationBean<>();
    registrationBean.setFilter(new HeadersFilter());
    registrationBean.setName("HeadersFilter");
    registrationBean.addUrlPatterns("/*");
    return registrationBean;
  }
}
