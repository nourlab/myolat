package io.gitlab.myolat.app;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(properties = "olat.debug=false")
class ApplicationTest {

  @Test
  void contextLoads() {
    // Nothing to do here
  }
}
