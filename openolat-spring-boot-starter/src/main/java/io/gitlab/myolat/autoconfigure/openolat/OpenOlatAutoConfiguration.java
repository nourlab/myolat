package io.gitlab.myolat.autoconfigure.openolat;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.*;

import javax.annotation.PostConstruct;

@Configuration
@Import({
  ServletConfiguration.class,
  ServletFilterConfiguration.class,
  ServletListenerConfiguration.class
})
@ImportResource("classpath:/openolat-context.xml")
@Slf4j
public class OpenOlatAutoConfiguration {

  @Profile("development")
  @Configuration
  @PropertySource({
    "classpath:/serviceconfig/olat.properties",
    "classpath:/olat.development.properties"
  })
  static class DevelopmenOpenOlatConfiguration {}

  @Profile("load-test")
  @Configuration
  @PropertySource({
    "classpath:/serviceconfig/olat.properties",
    "classpath:/olat.loadtest.properties"
  })
  static class LoadTestOpenOlatConfiguration {

    @Bean
    public TestUsersCustomizer testUsersCustomizer() {
      return new TestUsersCustomizer();
    }
  }

  @Profile("production")
  @Configuration
  @PropertySource({
    "classpath:/serviceconfig/olat.properties",
    "classpath:/olat.production.properties"
  })
  static class ProductionOpenOlatConfiguration {}

  @PostConstruct
  public void init() {
    log.info("OpenOlat module successfully initialized");
  }

  @Bean
  public ExplodeStaticResourcesListener explodeStaticResourcesListener() {
    return new ExplodeStaticResourcesListener();
  }

  @Bean
  public ResetFullPathToWebappSrcCustomizer fullPathToWebappSrcCustomizer() {
    return new ResetFullPathToWebappSrcCustomizer();
  }
}
